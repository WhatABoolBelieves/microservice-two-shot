from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json

from .models import Hat

# Create your views here.


# Make one function view to show the list of your model. Whenever you need a point of reference about how to do something for the RESTful APIs, look at your code in Conference GO! or the Wardrobe API in the project.

# Configure the view in a URLs file that you will have to create for your Django app.

@require_http_methods(["GET", "POST"])
def list_hats(request):
    if request.method == "GET":
        hats = Hat.objects.all()
        return JsonResponse(
            {"hats": hats},
        )
    content = json.loads(request.body)
    hat = Hat.objects.create(**content)
    return JsonResponse(
      hat,
      safe=False,
    )

@require_http_methods(["DELETE"])
def show_hat(request, pk):
    try:
        hat = Hat.objects.get(id=pk)
        hat.delete()
        return JsonResponse(
            hat,
            safe=False,
        )
    except Hat.DoesNotExist:
        return JsonResponse({"message": "Does not exist"})