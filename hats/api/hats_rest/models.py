from django.db import models

# Create your models here.


class LocationVO(models.Model):
    closet_name = models.CharField(max_length=100)
    section_number = models.PositiveSmallIntegerField()
    shelf_number = models.PositiveSmallIntegerField()

    import_href = models.CharField(max_length=200, unique=True)

class Hat(models.Model):

  name = models.CharField(max_length=200, blank=True)
  fabric = models.CharField(max_length=100, blank=True)
  style_name = models.CharField(max_length=100, blank=True)
  color = models.CharField(max_length=50, blank=True)
  picture_URL = models.URLField(blank=True)

  wardrobe_location = models.ForeignKey(
        LocationVO,
        related_name="hats",
        on_delete=models.CASCADE,
    )