from .views import list_hats
from django.urls import path, include

urlpatterns = [
  path("hats/", list_hats, name="list_hats"),
]

